# cardboard-bot-server

[![](https://gitlab.com/ok-ak/cardboard-bot-server/badges/master/pipeline.svg)](https://gitlab.com/ok-ak/cardboard-bot-server/commits/master)

[![](https://gitlab.com/ok-ak/cardboard-bot-server/badges/master/coverage.svg)](https://gitlab.com/ok-ak/cardboard-bot-server/commits/master)

**WORK IN PROGRESS!!!**

Raspberry Pi project using a Node server with Websockets and pigpio library.

Goal is to have the Raspberry Pi control servos and leds through PWM and simple digital writes (on/off) on a battle-bot. The Pi recieves user input via WebSockets JSON packets matching the Message class object.

## Installation Instructions
1. Before you download this repo, you will need to download and install the C pigpio library
    - [C pigpio library download and instructions link](http://abyz.me.uk/rpi/pigpio/download.html)
    - Any of the methods should work. For what its worth, I used Method 1 for my personal machine.
    - After installation, you should see a lot of verbage on the screen. Make sure there are no words saying failure, failed, error or any other negative words indicating installation was not successful. Be sure to read the installation instructions for troubleshooting in the event you encounter an error.
2. Now that you have successfully install C pigpio library on your device you can now download and install this repo to you device as well.
    - Open your terminal by pressing `CTRL + ALT + T` or by searching for your terminal in your list of programs.
    - In your terminal, type `git clone https://gitlab.com/ok-ak/cardboard-bot-server.git`
3. In the same terminal, type `cd cardboard-bot-server`
4. In the same terminal, type `npm install`
5. At this point the app is ready to run
    - To start the app, and in the same terminal, type `npm run start`
    - You will see terminal working and eventually reply back to you with a message indicating which localhost:port-number the application is running on.
    - You're all set!

NOTE: This app is a two part system. You've just install the server-side of the app. You will need to also install the client-side which can be found at [Cardboard-Bot-UI](https://gitlab.com/ok-ak/cardboard-bot-ui). Refer to that repo for client-side installation instructions.

NOTE: This server-side code requires node, npm, C, and python3.

NOTE: This server-side app was install on a Raspberry Pi 3 B+ device. Technically this should work on any supported Raspberry Pi device per instructions provided at npm pigpio library. There is no guarentee that this code will work on a non-Raspberry Pi device.