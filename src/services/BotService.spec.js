'use strict'
import BotService, { MAX_PULSE_WIDTH, DEFAULT_DUTY_CYCLE_PERIOD, DIGITAL_ON } from './BotService';
import Message from '../dto/Message';
import pigpio from 'pigpio';

//Mocks
jest.mock('pigpio');
let Gpio;
let botService;


describe('botService', () => {

  beforeAll(() => {
    Gpio = pigpio.Gpio;
    Gpio.OUTPUT = 1;
  });

  beforeEach(() => {
    botService = new BotService();
  })

  afterEach(() => {
    jest.clearAllMocks();
  })

  describe('_buttonCommand', () => {
    test('with Message.pins.button missing, then return error "Button requires Message.pins.button property"', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          button: [[8000, 8080], [9000, 9001]]
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [
        "Button requires Message.pins.button property"
      ];

      //when
      message.pins.button = undefined;

      const actualErrors: string[] = botService._buttonCommand(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

    test('with Message.action missing, then return error "Button requires Message.action property"', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          button: [[8000, 8080], [9000, 9001]]
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [
        "Button requires Message.action property"
      ];

      //when
      message.action = undefined;

      const actualErrors: string[] = botService._buttonCommand(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

    test('with valid message containing "holdOnOrOffFlipStateWhenPressedAgain", then update BotService.controls with button pins', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          button: [4444, 443]
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedGpioSetPinA = [message.pins.button[0], {mode: Gpio.OUTPUT}];
      const expectedGpioSetPinB = [message.pins.button[1], {mode: Gpio.OUTPUT}];
      
      const expectedControlKeys = [
        message.pins.button
      ];
      
      //when
      const mockGpioOutput = jest.fn(() => {
        this.OUTPUT = 1;
      });

      const actualErrors = botService._buttonCommand(message);

      const actualControlsState = botService.controls;

      //then
      expect(actualErrors).toEqual([]);
      expect(Gpio.mock.calls[0]).toEqual(expectedGpioSetPinA);
      expect(Gpio.mock.calls[1]).toEqual(expectedGpioSetPinB);
      expect(Gpio).toHaveBeenCalledTimes(2);
      
      expect(Gpio.mock.instances[0].digitalWrite).toHaveBeenCalledTimes(1);
      expect(Gpio.mock.instances[1].digitalWrite).toHaveBeenCalledTimes(1);
      expect(Gpio.mock.instances[0].pwmWrite).toHaveBeenCalledTimes(0);
      expect(Gpio.mock.instances[1].pwmWrite).toHaveBeenCalledTimes(0);

      const actualControlsKeys = [...actualControlsState.keys()]
      actualControlsKeys.forEach((control, index) => {
        expect(control).toEqual(expectedControlKeys[index])
      });

      expect(actualControlsKeys.length).toEqual(expectedControlKeys.length);

      done();
    });

    test('with valid message containing "slowOscillateOnOffUntilPressedAgain", then update BotService.controls with button pins', (done) => {
      //given
      const message: Message = new Message(
        'slowOscillateOnOffUntilPressedAgain',
        'some description',
        {
          button: [4444, 443]
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedGpioSetPinA = [message.pins.button[0], {mode: Gpio.OUTPUT}];
      const expectedGpioSetPinB = [message.pins.button[1], {mode: Gpio.OUTPUT}];
      
      const expectedControlKeys = [
        message.pins.button
      ];
      
      //when
      const mockGpioOutput = jest.fn(() => {
        this.OUTPUT = 1;
      });
                
      jest.useFakeTimers();

      const actualErrors = botService._buttonCommand(message);

      jest.advanceTimersByTime(DEFAULT_DUTY_CYCLE_PERIOD);

      const actualControlsState = botService.controls;


      //then
      expect(actualErrors).toEqual([]);
      expect(Gpio.mock.calls[0]).toEqual(expectedGpioSetPinA);
      expect(Gpio.mock.calls[1]).toEqual(expectedGpioSetPinB);
      expect(Gpio).toHaveBeenCalledTimes(2);

      expect(Gpio.mock.instances[0].digitalWrite).toHaveBeenCalledTimes(0);
      expect(Gpio.mock.instances[1].digitalWrite).toHaveBeenCalledTimes(0);
      expect(Gpio.mock.instances[0].pwmWrite).toHaveBeenCalledTimes(1);
      expect(Gpio.mock.instances[1].pwmWrite).toHaveBeenCalledTimes(1);

      const actualControlsKeys = [...actualControlsState.keys()]
      actualControlsKeys.forEach((control, index) => {
        expect(control).toEqual(expectedControlKeys[index])
      });

      expect(actualControlsKeys.length).toEqual(expectedControlKeys.length);
      done();
    });

    test('with valid message containing "fastOscillateOnOffUntilPressedAgain", then update BotService.controls with button pins', (done) => {
      //given
      const message: Message = new Message(
        'fastOscillateOnOffUntilPressedAgain',
        'some description',
        {
          button: [4444, 443]
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedGpioSetPinA = [message.pins.button[0], {mode: Gpio.OUTPUT}];
      const expectedGpioSetPinB = [message.pins.button[1], {mode: Gpio.OUTPUT}];
      
      const expectedControlKeys = [
        message.pins.button
      ];
      
      //when
      const mockGpioOutput = jest.fn(() => {
        this.OUTPUT = 1;
      });
                
      jest.useFakeTimers();

      const actualErrors = botService._buttonCommand(message);

      jest.advanceTimersByTime(DEFAULT_DUTY_CYCLE_PERIOD);

      const actualControlsState = botService.controls;


      //then
      expect(actualErrors).toEqual([]);
      expect(Gpio.mock.calls[0]).toEqual(expectedGpioSetPinA);
      expect(Gpio.mock.calls[1]).toEqual(expectedGpioSetPinB);
      expect(Gpio).toHaveBeenCalledTimes(2);

      expect(Gpio.mock.instances[0].digitalWrite).toHaveBeenCalledTimes(0);
      expect(Gpio.mock.instances[1].digitalWrite).toHaveBeenCalledTimes(0);
      expect(Gpio.mock.instances[0].pwmWrite).toHaveBeenCalledTimes(1);
      expect(Gpio.mock.instances[1].pwmWrite).toHaveBeenCalledTimes(1);

      const actualControlsKeys = [...actualControlsState.keys()]
      actualControlsKeys.forEach((control, index) => {
        expect(control).toEqual(expectedControlKeys[index])
      });

      expect(actualControlsKeys.length).toEqual(expectedControlKeys.length);
      done();
    });
  });

  describe('_calculateYaxisDirections', () => {
    test('with FORWARD only input returns +1 values for both left and right Y-axis', () => {
      //given
      const x = 0;
      const y = 1;

      const expectedYaxisDirectionLeft = 1;
      const expectedYaxisDirectionRight = 1;

      //when
      const [actualYaxisDirectionLeft, actualYaxisDirectionRight] = 
          botService._calculateYaxisDirections(x, y);

      //then
      expect(actualYaxisDirectionLeft).toEqual(expectedYaxisDirectionLeft);
      expect(actualYaxisDirectionRight).toEqual(expectedYaxisDirectionRight);

    });

    test('with 45deg input returns +1 values for both left and right Y-axis', () => {
      //given
      const x = 1;
      const y = 1;

      const expectedYaxisDirectionLeft = 1;
      const expectedYaxisDirectionRight = 1;

      //when
      const [actualYaxisDirectionLeft, actualYaxisDirectionRight] = 
          botService._calculateYaxisDirections(x, y);

      //then
      expect(actualYaxisDirectionLeft).toEqual(expectedYaxisDirectionLeft);
      expect(actualYaxisDirectionRight).toEqual(expectedYaxisDirectionRight);

    });

    test('with 225deg only input returns -1 values for both left and right Y-axis', () => {
      //given
      const x = -1;
      const y = -1;

      const expectedYaxisDirectionLeft = -1;
      const expectedYaxisDirectionRight = -1;

      //when
      const [actualYaxisDirectionLeft, actualYaxisDirectionRight] = 
          botService._calculateYaxisDirections(x, y);

      //then
      expect(actualYaxisDirectionLeft).toEqual(expectedYaxisDirectionLeft);
      expect(actualYaxisDirectionRight).toEqual(expectedYaxisDirectionRight);

    });

    test('with RIGHT only input returns +1 left and -1 right Y-axis', () => {
      //given
      const x = 1;
      const y = 0;

      const expectedYaxisDirectionLeft = 1;
      const expectedYaxisDirectionRight = -1;

      //when
      const [actualYaxisDirectionLeft, actualYaxisDirectionRight] = 
          botService._calculateYaxisDirections(x, y);

      //then
      expect(actualYaxisDirectionLeft).toEqual(expectedYaxisDirectionLeft);
      expect(actualYaxisDirectionRight).toEqual(expectedYaxisDirectionRight);

    });

    test('with RIGHT only input returns -1 left and +1 right Y-axis', () => {
      //given
      const x = -1;
      const y = 0;

      const expectedYaxisDirectionLeft = -1;
      const expectedYaxisDirectionRight = 1;

      //when
      const [actualYaxisDirectionLeft, actualYaxisDirectionRight] = 
          botService._calculateYaxisDirections(x, y);

      //then
      expect(actualYaxisDirectionLeft).toEqual(expectedYaxisDirectionLeft);
      expect(actualYaxisDirectionRight).toEqual(expectedYaxisDirectionRight);

    });
  });

  describe('_calculatePulseWidthPair', () => {
    test('with max FORWARD only input returns maximum pulse widths for both elements in the pair', (done) => {
      //given
      const x = 0;
      const y = 7;
      const scale_max = y;

      const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
      const expectedPulseWidthRight = MAX_PULSE_WIDTH;

      //when
      const [actualPulseWidthLeft, actualPulseWidthRight] = 
          botService._calculatePulseWidthPair(x, y, scale_max);

      //then
      expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
      expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);

      done();
    });

    test('with 1% FORWARD only input returns 1% max pulse widths for both elements in the pair', (done) => {
      //given
      const x = 0;
      const y = 1;
      const scale_max = 100;

      const expectedPulseWidthLeft = Math.floor(MAX_PULSE_WIDTH / 100);
      const expectedPulseWidthRight = Math.floor(MAX_PULSE_WIDTH / 100);

      //when
      const [actualPulseWidthLeft, actualPulseWidthRight] = 
          botService._calculatePulseWidthPair(x, y, scale_max);

      //then
      expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
      expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);

      done();
    });

    test('with 30.001deg angle and 1.00 scalar should return 100% left pulse width and 0% right pulse width in the pair', (done) => {
      //given
      const degToRad = Math.PI / 180;
      const rad = 30.001 * degToRad;
      const hypotenus = 1;
      const x = hypotenus * Math.cos(rad);
      const y = hypotenus * Math.sin(rad);
      const scale_max = 1;

      const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
      const expectedPulseWidthRight = 0;

      //when
      const [actualPulseWidthLeft, actualPulseWidthRight] = 
          botService._calculatePulseWidthPair(x, y, scale_max);

      //then
      expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
      expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);

      done();
    });

    test('with 329.999deg angle and 1.00 scalar should return 100% left pulse width and 0% right pulse width in the pair', (done) => {
      //given
      const degToRad = Math.PI / 180;
      const rad = 329.99 * degToRad;
      const hypotenus = 1;
      const x = hypotenus * Math.cos(rad);
      const y = hypotenus * Math.sin(rad);
      const scale_max = 1;

      const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
      const expectedPulseWidthRight = 0;

      //when
      const [actualPulseWidthLeft, actualPulseWidthRight] = 
          botService._calculatePulseWidthPair(x, y, scale_max);

      //then
      expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
      expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);

      done();
    });

    test('with 149.99deg angle and 1.00 scalar should return 0% left pulse width and 100% right pulse width in the pair', (done) => {
      //given
      const degToRad = Math.PI / 180;
      const rad = 149.99 * degToRad;
      const hypotenus = 1;
      const x = hypotenus * Math.cos(rad);
      const y = hypotenus * Math.sin(rad);
      const scale_max = 1;

      const expectedPulseWidthLeft = 0;
      const expectedPulseWidthRight = MAX_PULSE_WIDTH;

      //when
      const [actualPulseWidthLeft, actualPulseWidthRight] = 
          botService._calculatePulseWidthPair(x, y, scale_max);

      //then
      expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
      expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);

      done();
    });

    test('with 210.01deg angle and 1.00 scalar should return 0% left pulse width and 100% right pulse width in the pair', (done) => {
      //given
      const degToRad = Math.PI / 180;
      const rad = 210.01 * degToRad;
      const hypotenus = 1;
      const x = hypotenus * Math.cos(rad);
      const y = hypotenus * Math.sin(rad);
      const scale_max = 1;

      const expectedPulseWidthLeft = 0;
      const expectedPulseWidthRight = MAX_PULSE_WIDTH;

      //when
      const [actualPulseWidthLeft, actualPulseWidthRight] = 
          botService._calculatePulseWidthPair(x, y, scale_max);

      //then
      expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
      expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);

      done();
    });

    test('with 240deg angle and 1.00 scalar should return 50% left pulse width and 100% right pulse width in the pair', (done) => {
      //given
      const degToRad = Math.PI / 180;
      const rad = 240 * degToRad;
      const hypotenus = 1;
      const x = hypotenus * Math.cos(rad);
      const y = hypotenus * Math.sin(rad);
      const scale_max = 1;

      const expectedPulseWidthLeft = MAX_PULSE_WIDTH / 2;
      const expectedPulseWidthRight = MAX_PULSE_WIDTH;

      //when
      const [actualPulseWidthLeft, actualPulseWidthRight] = 
          botService._calculatePulseWidthPair(x, y, scale_max);

      //then
      expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
      expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);

      done();
    });

    test('with 60deg angle and 1.00 scalar should return 100% left pulse width and 50% right pulse width in the pair', (done) => {
      //given
      const degToRad = Math.PI / 180;
      const rad = 60 * degToRad;
      const hypotenus = 1;
      const x = hypotenus * Math.cos(rad);
      const y = hypotenus * Math.sin(rad);
      const scale_max = 1;

      const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
      const expectedPulseWidthRight = MAX_PULSE_WIDTH / 2;

      //when
      const [actualPulseWidthLeft, actualPulseWidthRight] = 
          botService._calculatePulseWidthPair(x, y, scale_max);

      //then
      expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
      expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);

      done();
    });

    //example of U-turns
    describe('with RIGHT U-turn inputs', () => {
      test('when xy vector at 30deg and maximum scalar will return max pulse width pair', (done) => {
        //given
        const x = Math.sqrt(3) / 2 + 0.0001;
        const y = 0.5;
        const scale_max = 1;
  
        const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
        const expectedPulseWidthRight = MAX_PULSE_WIDTH;
  
        //when
        const [actualPulseWidthLeft, actualPulseWidthRight] = 
            botService._calculatePulseWidthPair(x, y, scale_max);
  
        //then
        expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
        expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);
  
        done();
      });

      test('when xy vector at 330deg and maximum scalar will return max pulse width pair', (done) => {
        //given
        const x = Math.sqrt(3) / 2 + 0.0001;
        const y = -0.5;
        const scale_max = 1;
  
        const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
        const expectedPulseWidthRight = MAX_PULSE_WIDTH;
  
        //when
        const [actualPulseWidthLeft, actualPulseWidthRight] = 
            botService._calculatePulseWidthPair(x, y, scale_max);
  
        //then
        expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
        expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);
  
        done();
      });

      test('when xy vector at 150deg and maximum scalar will return max pulse width pair', (done) => {
        //given
        const x = -Math.sqrt(3) / 2 - 0.0001;
        const y = 0.5;
        const scale_max = 1;
  
        const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
        const expectedPulseWidthRight = MAX_PULSE_WIDTH;
  
        //when
        const [actualPulseWidthLeft, actualPulseWidthRight] = 
            botService._calculatePulseWidthPair(x, y, scale_max);
  
        //then
        expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
        expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);
  
        done();
      });

      test('when xy vector at 210deg and maximum scalar will return max pulse width pair', (done) => {
        //given
        const x = -Math.sqrt(3) / 2 - 0.0001;
        const y = -0.5;
        const scale_max = 1;
  
        const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
        const expectedPulseWidthRight = MAX_PULSE_WIDTH;
  
        //when
        const [actualPulseWidthLeft, actualPulseWidthRight] = 
            botService._calculatePulseWidthPair(x, y, scale_max);
  
        //then
        expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
        expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);
  
        done();
      });

      test('with max RIGHT only input returns maximum pulse widths for both elements in the pair', (done) => {
        //given
        const x = 5;
        const y = 0;
        const scale_max = x;
  
        const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
        const expectedPulseWidthRight = MAX_PULSE_WIDTH;
  
        //when
        const [actualPulseWidthLeft, actualPulseWidthRight] = 
            botService._calculatePulseWidthPair(x, y, scale_max);
  
        //then
        expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
        expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);
  
        done();
      });
  
      test('with 1% RIGHT only input returns 1% max pulse widths for both elements in the pair', (done) => {
        //given
        const x = 1;
        const y = 0;
        const scale_max = 100;
  
        const expectedPulseWidthLeft = Math.floor(MAX_PULSE_WIDTH / 100);
        const expectedPulseWidthRight = Math.floor(MAX_PULSE_WIDTH / 100);
  
        //when
        const [actualPulseWidthLeft, actualPulseWidthRight] = 
            botService._calculatePulseWidthPair(x, y, scale_max);
  
        //then
        expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
        expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);
  
        done();
      });
  
      test('with max LEFT only input returns maximum pulse widths for both elements in the pair', (done) => {
        //given
        const x = -5;
        const y = 0;
        const scale_max = Math.abs(x);
  
        const expectedPulseWidthLeft = MAX_PULSE_WIDTH;
        const expectedPulseWidthRight = MAX_PULSE_WIDTH;
  
        //when
        const [actualPulseWidthLeft, actualPulseWidthRight] = 
            botService._calculatePulseWidthPair(x, y, scale_max);
  
        //then
        expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
        expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);
  
        done();
      });

      test('with 10deg angle and 0.5 scalar should return half pulse widths for both elements in the pair', (done) => {
        //given
        const degToRad = Math.PI / 180;
        const rad = 10 * degToRad;
        const hypotenus = 1;
        const x = hypotenus * Math.cos(rad);
        const y = hypotenus * Math.sin(rad);
        const scale_max = 2;
  
        const expectedPulseWidthLeft = MAX_PULSE_WIDTH / 2;
        const expectedPulseWidthRight = MAX_PULSE_WIDTH / 2;
  
        //when
        const [actualPulseWidthLeft, actualPulseWidthRight] = 
            botService._calculatePulseWidthPair(x, y, scale_max);
  
        //then
        expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
        expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);
  
        done();
      });

      test('with 195deg angle and 0.25 scalar should return quarter pulse widths for both elements in the pair', (done) => {
        //given
        const degToRad = Math.PI / 180;
        const rad = 195 * degToRad;
        const hypotenus = 1;
        const x = hypotenus * Math.cos(rad);
        const y = hypotenus * Math.sin(rad);
        const scale_max = 4;
  
        const expectedPulseWidthLeft = MAX_PULSE_WIDTH / 4;
        const expectedPulseWidthRight = MAX_PULSE_WIDTH / 4;
  
        //when
        const [actualPulseWidthLeft, actualPulseWidthRight] = 
            botService._calculatePulseWidthPair(x, y, scale_max);
  
        //then
        expect(actualPulseWidthLeft).toEqual(expectedPulseWidthLeft);
        expect(actualPulseWidthRight).toEqual(expectedPulseWidthRight);
  
        done();
      });
    });
    
  });

  describe('_clearPreviousServoIntervals', () => {
    test('sets all matching servo pin groups under servo.left and servo.right in a servo obj identified in BotService.controls to null', (done) => {
      //given
      let servo: Servo = {
        left: [[8000, 8080], [9000, 9001]],
        right: [[1000, 1001], [2000, 2001]]
      }

      botService.controls.set(servo.left, setInterval(() => {}, 0));
      botService.controls.set(servo.right, setInterval(() => {}, 0));
      
      //when
      botService._clearExistingServoControls(servo);

      const afterControlState = botService.controls;

      //then
      const keys: string[] = [...afterControlState.keys()];
      expect(keys.length).toEqual(2);
      keys.forEach((key) => {
        expect(afterControlState.get(key)).toEqual(null);
      });

      done();
    });

    test('sets all matching servo pin groups under servo.single in a servo obj identified in BotService.controls to null', (done) => {
      //given
      let servo: Servo = {
        single: [[8000, 8080], [9000, 9001]]
      }

      botService.controls.set(servo.single, setInterval(() => {}, 0));
      
      //when
      botService._clearExistingServoControls(servo);

      const afterControlState = botService.controls;

      //then
      const keys: string[] = [...afterControlState.keys()];
      expect(keys.length).toEqual(1);
      keys.forEach((key) => {
        expect(afterControlState.get(key)).toEqual(null);
      });

      done();
    });
  });

  describe('_servoCommand', () => {
    test('is missing Message.pins.servo, then returns error "Servo requires Message.pins.servo property" and does not update BotService.controls', (done) => {
      //given
      let message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const beforeControlState = botService.controls;

      const expectedErrors: string[] = ["Servo requires Message.pins.servo property"];

      message.pins.servo = undefined

      //when
      const actualErrors: string[] = botService._servoCommand(message);

      const afterControlState = botService.controls;

      //then
      expect(actualErrors).toEqual(expectedErrors);
      expect(Gpio).toHaveBeenCalledTimes(0);
      expect(afterControlState).toEqual(beforeControlState);

      done();
    });

    test('is missing Message.pins.servo.left, then returns error "Servo requires Message.pins.servo.left AND Message.pins.servo.right OR Message.pins.servo.single properties defined." and does not update BotService.controls', (done) => {
      //given
      let message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const beforeControlState = botService.controls;

      const expectedErrors: string[] = ["Servo requires Message.pins.servo.left AND Message.pins.servo.right OR Message.pins.servo.single properties defined."];

      message.pins.servo.left = undefined

      //when
      const actualErrors: string[] = botService._servoCommand(message);

      const afterControlState = botService.controls;

      //then
      expect(actualErrors).toEqual(expectedErrors);
      expect(Gpio).toHaveBeenCalledTimes(0);
      expect(afterControlState).toEqual(beforeControlState);

      done();
    });

    test('is missing Message.pins.servo.right, then returns error "Servo requires Message.pins.servo.left AND Message.pins.servo.right OR Message.pins.servo.single properties defined." and does not update BotService.controls', (done) => {
      //given
      let message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const beforeControlState = botService.controls;

      const expectedErrors: string[] = ["Servo requires Message.pins.servo.left AND Message.pins.servo.right OR Message.pins.servo.single properties defined."];

      message.pins.servo.right = undefined

      //when
      const actualErrors: string[] = botService._servoCommand(message);

      const afterControlState = botService.controls;

      //then
      expect(actualErrors).toEqual(expectedErrors);
      expect(Gpio).toHaveBeenCalledTimes(0);
      expect(afterControlState).toEqual(beforeControlState);

      done();
    });

    test('is missing Message.x, then returns error "Servo required Message.x property" and does not update BotService.controls', (done) => {
      //given
      let message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const beforeControlState = botService.controls;

      const expectedErrors: string[] = ["Servo requires Message.x property defined."];

      message.x = undefined

      //when
      const actualErrors: string[] = botService._servoCommand(message);

      const afterControlState = botService.controls;

      //then
      expect(actualErrors).toEqual(expectedErrors);
      expect(Gpio).toHaveBeenCalledTimes(0);
      expect(afterControlState).toEqual(beforeControlState);

      done();
    });

    test('is missing Message.y, then returns error "Servo required Message.y property" and does not update BotService.controls', (done) => {
      //given
      let message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const beforeControlState = botService.controls;

      const expectedErrors: string[] = ["Servo requires Message.y property defined."];

      message.y = undefined

      //when
      const actualErrors: string[] = botService._servoCommand(message);

      const afterControlState = botService.controls;

      //then
      expect(actualErrors).toEqual(expectedErrors);
      expect(Gpio).toHaveBeenCalledTimes(0);
      expect(afterControlState).toEqual(beforeControlState);

      done();
    });

    test('is missing Message.scale_max, then returns error "Servo required Message.scale_max property" and does not update BotService.controls', (done) => {
      //given
      let message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const beforeControlState = botService.controls;

      const expectedErrors: string[] = ["Servo requires Message.scale_max property defined."];

      message.scale_max = undefined

      //when
      const actualErrors: string[] = botService._servoCommand(message);

      const afterControlState = botService.controls;

      //then
      expect(actualErrors).toEqual(expectedErrors);
      expect(Gpio).toHaveBeenCalledTimes(0);
      expect(afterControlState).toEqual(beforeControlState);

      done();
    });

    test('is passed in with 1 left and 1 right pin arrays, then botService.controls adds two new intervals where each interval matches the pin sets', async (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedGpioSetPinLeftA = [message.pins.servo.left[0][0], {mode: Gpio.OUTPUT}];
      const expectedGpioSetPinLeftB = [message.pins.servo.left[1][0], {mode: Gpio.OUTPUT}];
      const expectedGpioSetPinRightA = [message.pins.servo.right[0][0], {mode: Gpio.OUTPUT}];
      const expectedGpioSetPinRightB = [message.pins.servo.right[1][0], {mode: Gpio.OUTPUT}];
      
      const expectedControlKeys = [
        message.pins.servo.left,
        message.pins.servo.right
      ]
      
      //when
      const mockGpioOutput = jest.fn(() => {
        this.OUTPUT = 1;
      });
                
      jest.useFakeTimers();

      const actualErrors = botService._servoCommand(message);

      jest.advanceTimersByTime(DEFAULT_DUTY_CYCLE_PERIOD);

      const actualControlsState = botService.controls;


      //then
      expect(actualErrors).toEqual([]);
      expect(Gpio.mock.calls[0]).toEqual(expectedGpioSetPinLeftA);
      expect(Gpio.mock.calls[1]).toEqual(expectedGpioSetPinLeftB);
      expect(Gpio.mock.calls[2]).toEqual(expectedGpioSetPinRightA);
      expect(Gpio.mock.calls[3]).toEqual(expectedGpioSetPinRightB);
      expect(Gpio).toHaveBeenCalledTimes(4);

      const actualControlsKeys = [...actualControlsState.keys()]
      actualControlsKeys.forEach((control, index) => {
        expect(control).toEqual(expectedControlKeys[index])
      });

      expect(actualControlsKeys.length).toEqual(expectedControlKeys.length);
      done();
    })
  })

  describe('command', () => {
    test('is missing a Message.pins object and returns two errors', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [
        "Servos require Message.pins.servo object, Message.x, Message.y and Message.scale_max",
        "Buttons require Message.pins.button object, Message.action and Message.scale_max"
      ];
      
      //when
      message.pins = undefined;
      const actualErrors: string[] = botService.command(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

    test('is missing a Message.x object and returns two errors', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [
        "Servos require Message.pins.servo object, Message.x, Message.y and Message.scale_max",
        "Buttons require Message.pins.button object, Message.action and Message.scale_max"
      ];
      
      //when
      message.x = undefined;
      const actualErrors: string[] = botService.command(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

    test('is missing a Message.y object and returns two errors', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [
        "Servos require Message.pins.servo object, Message.x, Message.y and Message.scale_max",
        "Buttons require Message.pins.button object, Message.action and Message.scale_max"
      ];
      
      //when
      message.y = undefined;
      const actualErrors: string[] = botService.command(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

    test('is missing a Message.scale_max object and returns two errors', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [
        "Servos require Message.pins.servo object, Message.x, Message.y and Message.scale_max",
        "Buttons require Message.pins.button object, Message.action and Message.scale_max"
      ];
      
      //when
      message.scale_max = undefined;
      const actualErrors: string[] = botService.command(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

    test('is missing a Message.pins.servo object and returns two errors', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [
        "Servos require Message.pins.servo object, Message.x, Message.y and Message.scale_max",
        "Buttons require Message.pins.button object, Message.action and Message.scale_max"
      ];
      
      //when
      message.pins.servo = undefined;
      const actualErrors: string[] = botService.command(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

    test('is missing a Message.pins.button object and returns two errors', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          button: [8000, 8080, 9000, 9001]
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [
        "Servos require Message.pins.servo object, Message.x, Message.y and Message.scale_max",
        "Buttons require Message.pins.button object, Message.action and Message.scale_max"
      ];
      
      //when
      message.pins.button = undefined;
      const actualErrors: string[] = botService.command(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

    test('is a valid Message.pins.servo object and returns no errors', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          servo: {
            left: [[8000, 8080], [9000, 9001]],
            right: [[1000, 1001], [2000, 2001]]
          }
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [];

      //when
      const actualErrors: string[] = botService.command(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

    test('is a valid Message.pins.button object and returns no errors', (done) => {
      //given
      const message: Message = new Message(
        'holdOnOrOffFlipStateWhenPressedAgain',
        'some description',
        {
          button: [8000, 8080, 9000, 9001]
        },
        'some text',
        0,
        1,
        1,
        ['nunya']
      );

      const expectedErrors: string[] = [];
      
      //when
      const actualErrors: string[] = botService.command(message);

      //then
      expect(actualErrors).toEqual(expectedErrors);

      done();
    });

  });
});