//THIS NEEDS TO BE A CLASS NOW BECAUSE ITS GOING TO STORE STATE
//STATE PROPERTY WILL BE A MAP THAT KEY/PAIRS NUMBER[] WITH VIRTUAL BUTTON
//VIRTUAL BUTTONS CAN BE INTERRUPTED
//VIRTUAL BUTTONS WRAP SERVOS
//SERVOS ARE THE ONES THAT ACTUALLY USE THE NUMBER[]
//REFERENCE THE GITHUB REPO EXAMPLES FOR PIGPIO BUTTON INTERRUPTS AND SERVO CONTROL

'use strict'
//@flow
import Message, { type Pins,  type Servo, type ActionType } from "../dto/Message";
import { stringify } from "querystring";
import pigpio, { Gpio } from 'pigpio';

//max pulse width allowed for PWM duty cycle and comes from the pigpio library under function servoWrite(pulseWidth)
export const MAX_PULSE_WIDTH: number = 2500;

//default servoWrites operate at 50Hz. This equates to a 20ms period
export const DEFAULT_DUTY_CYCLE_PERIOD: number = 20;

export const DIGITAL_ON: number = 1;
export const DIGITAL_OFF: number = 0;
export const DEFAULT_PWM_DUTY_CYCLE: number = 255;

//RIGHT side boundaries of a cartesian circle, equates to bound a righthand U-turn
export const deg0AsRad: number = 0;
export const deg30AsRad: number = _round( Math.PI / 6, 4);
export const deg330AsRad: number = _round( 11 * Math.PI / 6, 4);
export const deg360AsRad: number = _round( 2 * Math.PI, 13);

//LEFT side boundaries of a cartesian circle, used to bound a lefthand U-turn
export const deg150AsRad: number = _round( 5 * Math.PI / 6, 4);
export const deg210AsRad: number = _round( 7 * Math.PI / 6, 4);

export const deg90AsRad: number = _round( Math.PI / 2, 4);
export const deg180AsRad: number = _round( Math.PI, 4);
export const deg270AsRad: number = _round( 3 * Math.PI / 2, 4);


function _round(num, places) {
  const factor = Math.pow(10, places);
  return Math.round( num * factor ) / factor;
}

export default class BotService {
  controls:  Map<number[] | number[][], IntervalID | Gpio[] | null>;

  constructor(){
    this.controls = new Map();
  }

  stopAllControls(): void {
    const keys: any[] = [...this.controls.keys()];
    keys.forEach((key) => {
      const value: any = this.controls.get(key);
      if(typeof value === typeof IntervalID){
        clearInterval(value);
      } else if (typeof value === typeof Gpio){
        value.digitalWrite(DIGITAL_OFF);
      }
    });
  }

  command(message: Message): string[] {
    let errors: string[] = [];

    if(typeof message.pins?.servo === "object" &&
        typeof message.x === "number" &&
        typeof message.y === "number" &&
        typeof message.scale_max === "number"){
      
      errors = [...this._servoCommand(message)];
    } else if (message.pins?.button){

      errors = [...this._buttonCommand(message)];
    } else {
      errors.push("Servos require Message.pins.servo object, Message.x, Message.y and Message.scale_max")
      errors.push("Buttons require Message.pins.button object, Message.action and Message.scale_max")
    }
  
    return errors;
  }

  _buttonCommand(message: Message): string[] {
    let errors: string[] = [];

    if(typeof message.action === "undefined"){
      errors.push("Button requires Message.action property");
      return errors;
    }

    const pins: Pins = message.pins;

    if(typeof pins.button === "undefined"){
      errors.push("Button requires Message.pins.button property");
      return errors;
    }

    const buttonPins: number[] = [...pins.button];
    let buttons: Gpio[] = [];
    if(message.action === "holdOnOrOffFlipStateWhenPressedAgain"){
      buttonPins.forEach((pin) => {
        const button: Gpio = new Gpio(pin, {mode: Gpio.OUTPUT});
        button.digitalWrite(DIGITAL_ON);
        buttons.push(button);
      });

      this.controls.set(buttonPins, buttons);
      
    } else if (message.action === "slowOscillateOnOffUntilPressedAgain"){
      buttonPins.forEach((pin) => {
        const button: Gpio = new Gpio(pin, {mode: Gpio.OUTPUT});
        let dutyCycle: number = DEFAULT_PWM_DUTY_CYCLE;
        const intervalId: IntervalID = setInterval(() => {
          button.pwmWrite(dutyCycle);
        
          dutyCycle += 5;
          if (dutyCycle > 255) {
            dutyCycle = 0;
          }
        }, DEFAULT_DUTY_CYCLE_PERIOD);
        
        buttons.push(button);
      });
      this.controls.set(buttonPins, buttons);
      
    } else if (message.action === "fastOscillateOnOffUntilPressedAgain"){
      buttonPins.forEach((pin) => {
        const button: Gpio = new Gpio(pin, {mode: Gpio.OUTPUT});
        let dutyCycle: number = DEFAULT_PWM_DUTY_CYCLE;
        const intervalId: IntervalID = setInterval(() => {
          button.pwmWrite(dutyCycle);
        
          dutyCycle += 30;
          if (dutyCycle > 255) {
            dutyCycle = 0;
          }
        }, DEFAULT_DUTY_CYCLE_PERIOD);
        
        buttons.push(button);
      });
      this.controls.set(buttonPins, buttons);
    }

    return errors;
  }

  _servoCommand(message: Message): string[] {
    let errors: string[] = [];

    const pins: Pins = message.pins;

    if(typeof pins.servo === "undefined"){
      errors.push("Servo requires Message.pins.servo property");
      return errors;
    }
    
    const servo: Servo = pins.servo;
    
    //clear existing interval ids for each pinGroup if they exist
    this._clearExistingServoControls(servo);
    
    const servoKeys: string[] = Object.keys(servo);

    if(typeof message.x === "undefined"){
      errors.push("Servo requires Message.x property defined.")
      return errors;
    }

    if(typeof message.y === "undefined"){
      errors.push("Servo requires Message.y property defined.")
      return errors;
    }

    if(typeof message.scale_max === "undefined"){
      errors.push("Servo requires Message.scale_max property defined.")
      return errors;
    }

    if(typeof servo.left === "undefined" || typeof servo.right === "undefined"){
      errors.push("Servo requires Message.pins.servo.left AND Message.pins.servo.right OR Message.pins.servo.single properties defined.")
      return errors;
    }
    
    const servoLeft: number[][] = servo.left;
    const servoRight: number[][] = servo.right;
    const x = message.x;
    const y = message.y;
    const scale_max = message.scale_max;

    const vectors: number[] = 
        this._calculateYaxisDirections(x, y);
    
    const pulseWidths: number[] = 
        this._calculatePulseWidthPair(x, y, scale_max);

    //set new interval ids for each pinGroup
    servoKeys.forEach((servoKey: string, servoIndex: number) => {
      const pinGroup: number[][] = servo[servoKey];
      
      const newIntervalId: IntervalID = setInterval(() => {
        pinGroup.forEach((pins: number[], pinIndex: number) => {
          this._createNewServoWrite(pins, vectors[servoIndex], pulseWidths[servoIndex]);
        })
      }, DEFAULT_DUTY_CYCLE_PERIOD);

      this.controls.set(pinGroup, newIntervalId);
    });

    return errors;
  }

  _clearExistingServoControls(servo: Servo): void{
    const servoKeys: string[] = Object.keys(servo);

    servoKeys.forEach((servoKey: string) => {
      const pinGroup: number[][] = servo[servoKey];

      if(this.controls.has(pinGroup)){
        const oldIntervalId: IntervalID = this.controls.get(pinGroup);
        clearInterval(oldIntervalId);
      }
      this.controls.set(pinGroup, null);
    });
  }

  _createNewServoWrite(pins: number[], vector: number, pulseWidth: number): void{
    const motorPin: number = vector > 0? pins[0] : pins[1];
    const motor: Gpio = new Gpio(motorPin, {mode: Gpio.OUTPUT});
    motor.servoWrite(pulseWidth);
  }

  _calculateYaxisDirections(x: number, y: number): number[] {
    let leftYaxisDirection: number = 0;
    let rightYaxisDirection: number = 0;

    const angle: number = this._calculateRadiansFromCartesianPair(x, y);
    
    //LEFT and RIGHT U-turns
    if((angle >= 0 && angle <= deg30AsRad)
       || (angle >= deg330AsRad && angle <= deg360AsRad)) {
      leftYaxisDirection = 1;
      rightYaxisDirection = -1;
    } else if(angle >= deg150AsRad && angle <= deg210AsRad) {
      leftYaxisDirection = -1;
      rightYaxisDirection = 1;
    } else if(y >= 0) {
      leftYaxisDirection = 1;
      rightYaxisDirection = 1;
    } else {
      leftYaxisDirection = -1;
      rightYaxisDirection = -1;
    }

    return [leftYaxisDirection, rightYaxisDirection]
  }

  _calculatePulseWidthPair(x: number, y: number, scale_max: number): number[]{
    let pulseWidthLeft: number = 0;
    let pulseWidthRight: number = 0;

    const scalar = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    const angle: number = this._calculateRadiansFromCartesianPair(x, y);
    
    //LEFT and RIGHT U-turns
    if((angle >= 0 && angle <= deg30AsRad)
       || (angle >= deg330AsRad && angle <= deg360AsRad)
       || (angle >= deg150AsRad && angle <= deg210AsRad)) {
      pulseWidthLeft = _round(scalar * MAX_PULSE_WIDTH / scale_max, 0);
      pulseWidthRight = pulseWidthLeft;
    } else {
      let [xAxisBiasLeft, xAxisBiasRight] = this._calculateAngleFactor(angle, x);

      pulseWidthLeft = _round(xAxisBiasLeft * scalar * MAX_PULSE_WIDTH / scale_max, 0);
      pulseWidthRight = _round(xAxisBiasRight * scalar * MAX_PULSE_WIDTH / scale_max, 0);
    }

    return [pulseWidthLeft, pulseWidthRight];
  }

  _calculateAngleFactor(angle: number, x: number){
    let leftFactor = 0;
    let rightFactor = 0;
    
    const turnRight: boolean = x >= 0;

    let acuteAngle: number = 0;

    if(angle >= deg270AsRad){
      acuteAngle = deg360AsRad - angle;
    } else if (angle >= deg180AsRad) {
      acuteAngle = angle - deg180AsRad;
    } else if (angle >= deg90AsRad) {
      acuteAngle = deg180AsRad - angle;
    } else {
      acuteAngle = angle;
    }

    const maxAngle: number = deg90AsRad - deg30AsRad;
    const adjustedAcuteAngle: number = acuteAngle - deg30AsRad;

    if(turnRight) {
      leftFactor = 1;
      rightFactor = (adjustedAcuteAngle) / maxAngle;
    } else {
      leftFactor = (adjustedAcuteAngle) / maxAngle;
      rightFactor = 1;
    }

    return [leftFactor, rightFactor];
  }

  _calculateXaxisFactor(x: number, y: number, scalar: number){
    let leftFactor = 0;
    let rightFactor = 0;
    
    const max_x = scalar * Math.cos(deg30AsRad);
    const turnRight: boolean = x >= 0;

    if(turnRight) {
      leftFactor = 1;
      rightFactor = (max_x - Math.abs(x)) / max_x;
    } else {
      leftFactor = (max_x - Math.abs(x)) / max_x;
      rightFactor = 1;
    }

    return [leftFactor, rightFactor];
  }

  _calculateRadiansFromCartesianPair(x: number, y: number): number {
    if(y >= 0){
      return _round(Math.atan2(y, x), 6);
    } else {
      return _round(deg360AsRad + Math.atan2(y, x), 4);
    }
  }
  
}