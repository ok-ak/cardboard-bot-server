//@flow
const WebSocket = require('ws');
import Message from './dto/Message';
import BotService from './services/BotService';

type Response = {
  message: Message,
  errors: string[]
}

export const PORT: number = 8080;

export let server: WebSocket = new WebSocket.Server({ port: PORT });

let botService: BotService = new BotService();

server.on('connection', function connection(ws): void {

  ws.on('open', function open(): void {
    console.log('Server is listening at localhost:', PORT);
  });

  ws.on('message', function incoming(jsonMessage: string): void {
    console.log('!!!!!!!!!!! server received the following message:', jsonMessage);
    const message: Message = JSON.parse(jsonMessage);
    const errors: string[] = botService.command(message);
    const response: Response = {
      message: message, 
      errors: errors
    }

    ws.send(JSON.stringify(response));
  });

  ws.on('close', function close(): void {
    console.log("Server side Websocket has been closed on localhost:", PORT);
  });
});